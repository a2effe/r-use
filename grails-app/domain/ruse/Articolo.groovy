package ruse

class Articolo {

    String descrizione
    Date dataConsegna
    Date dataPresa
    
    static constraints = {
        descrizione empty: false, maxSize: 4096;
        dataConsegna nullable: false;
        dataPresa nullable: true;
    }

    static belongsTo = [
        personaConsegna: Persona,
        personaPresa: Persona
    ]
}
