package ruse

class Persona {
    String cognome
    String nome
    Date data
    String comuneProvenienza
    String tipo
    String telefono
    String email

    static constraints = {
        cognome empty: false;
        nome empty: false;
        data nullable: false;
        comuneProvenienza empty: false;
        tipo empty:false;
        telefono empty:false;
        email empty:false, mail:true;
    }

    static hasMany = [
        articoliConsegnati: Articolo,
        articoliPresi: Articolo
    ]
}
